import {combineReducers} from 'redux'
import getDetailPoke from './detailPokemon'
import GetPokemon from './getPokemon'
import saveMyPokemon from './savePokemon'
import cekPokemon from './cekPokemon'
import getAllMyPokemon from './myPokemon'

export default combineReducers({
GetPokemon,
getDetailPoke,
saveMyPokemon,
cekPokemon,
getAllMyPokemon,
})