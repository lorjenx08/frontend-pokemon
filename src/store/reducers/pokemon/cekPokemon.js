const initialState = {
  loading: false,
  payload: null,
  error: false
}

const cekPokemon = (state=initialState, action) => {
  switch (action.type) {
    case 'GET_POKEMON_CEK_BEGIN':
      return {
        ...state,
        loading: true,
      }
     case 'GET_POKEMON_CEK_SUCCESS':
      return {
        ...state,
        payload: action.payload,
        loading: false,
      }
     case 'GET_POKEMON_CEK_FAILURE':
      return {
        ...state,
        payload: action.payload,
        loading: false,
      }
default:
  return state
  
  }
}

export default cekPokemon