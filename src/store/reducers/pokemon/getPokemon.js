const initialState = {
  loading: false,
  payload: null,
  error: false,
  total: 0,
}

const getPokemon = (state=initialState, action) => {
  switch (action.type) {
    case 'GET_POKEMON_BEGIN':
      return {
        ...state,
        loading: true,
      }
     case 'GET_POKEMON_SUCCESS':
      return {
        ...state,
        payload: action.payload,
        loading: false,
        total: action.total,
      }
     case 'GET_POKEMON_FAILURE':
      return {
        ...state,
        payload: action.payload,
        loading: false,
      }
default:
  return state
  
  }
}

export default getPokemon