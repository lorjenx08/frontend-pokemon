const initialState = {
  loading: false,
  payload: null,
  error: false
}

const getDetailPoke = (state=initialState, action) => {
  switch (action.type) {
    case 'GET_POKEMON_DETAIL_BEGIN':
      return {
        ...state,
        loading: true,
      }
     case 'GET_POKEMON_DETAIL_SUCCESS':
      return {
        ...state,
        payload: action.payload,
        loading: false,
      }
     case 'GET_POKEMON_DETAIL_FAILURE':
      return {
        ...state,
        payload: action.payload,
        loading: false,
      }
default:
  return state
  
  }
}

export default getDetailPoke