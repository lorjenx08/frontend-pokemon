const initialState = {
  loading: false,
  payload: null,
  error: false
}

const getAllMyPokemon = (state=initialState, action) => {
  switch (action.type) {
    case 'GET_MY_POKEMON_BEGIN':
      return {
        ...state,
        loading: true,
      }
     case 'GET_MY_POKEMON_SUCCESS':
      return {
        ...state,
        payload: action.payload,
        loading: false,
      }
     case 'GET_MY_POKEMON_FAILURE':
      return {
        ...state,
        payload: action.payload,
        loading: false,
      }
default:
  return state
  
  }
}

export default getAllMyPokemon