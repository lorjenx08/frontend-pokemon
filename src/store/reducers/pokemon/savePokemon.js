const initialState = {
  loading: false,
  success: null,
  error: false,
}

const saveMyPokemon = (state=initialState, action) => {
  switch (action.type) {
    case 'SAVE_POKEMON_BEGIN':
      return {
        ...state,
        loading: true,
      }
     case 'SAVE_POKEMON_SUCCESS':
      return {
        ...state,
        success: action.success,
        loading: false,
      }
     case 'SAVE_POKEMON_FAILURE':
      return {
        ...state,
        success: action.success,
        loading: false,
      }
default:
  return state
  
  }
}

export default saveMyPokemon