import { combineReducers } from 'redux'
import pokemonReducers from './reducers/pokemon'

export default combineReducers({
pokemon: pokemonReducers,
})