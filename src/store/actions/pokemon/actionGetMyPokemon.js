
const actionGetMyPokemon = () => async (dispatch) => {
  try {
    dispatch({
      type: 'GET_MY_POKEMON_BEGIN'
    })
    const response = await fetch(`http://localhost:5005/api/v1/my-pokemons`, {
      method: 'get', 
      headers: {
        'Content-Type': 'application/json'
      },
    });
    const data = await response.json()
    console.log(data);
    dispatch({
      type: 'GET_MY_POKEMON_SUCCESS',
      payload: data.data
    })
  } catch (error) {
    dispatch({
      type: 'GET_MY_POKEMON_FAILURE',
      error: error,
    })
  }
}

export default actionGetMyPokemon