
const actionGetCekPokemon = (name) => async (dispatch) => {
  try {
    dispatch({
      type: 'GET_POKEMON_CEK_BEGIN'
    })
    const response = await fetch(`http://localhost:5005/api/v1/my-pokemon/${name}`, {
      method: 'get', 
      headers: {
        'Content-Type': 'application/json'
      },
    });
    const data = await response.json()
    console.log(data);
    dispatch({
      type: 'GET_POKEMON_CEK_SUCCESS',
      payload: data.type
    })
  } catch (error) {
    dispatch({
      type: 'GET_POKEMON_CEK_FAILURE',
      error: error,
    })
  }
}

export default actionGetCekPokemon