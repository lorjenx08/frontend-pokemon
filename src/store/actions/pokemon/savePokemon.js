
const actionSavePokemon = (datas) => async (dispatch) => {
  try {
    dispatch({
      type: 'SAVE_POKEMON_BEGIN'
    })
    const response = await fetch(`http://localhost:5005/api/v1/pokemon`, {
      method: 'post', 
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(datas)
    });
    const data = await response.json()
    console.log(data);
    dispatch({
      type: 'SAVE_POKEMON_SUCCESS',
      success: data.status
    })
  } catch (error) {
    dispatch({
      type: 'SAVE_POKEMON_FAILURE',
      error: error,
    })
  }
}

export default actionSavePokemon