
const actionGetPokemonDetail = (name) => async (dispatch) => {
  try {
    dispatch({
      type: 'GET_POKEMON_DETAIL_BEGIN'
    })
    const response = await fetch(`http://localhost:5005/api/v1/pokemon-detail/${name}`, {
      method: 'get', 
      headers: {
        'Content-Type': 'application/json'
      },
    });
    const data = await response.json()
    console.log(data);
    dispatch({
      type: 'GET_POKEMON_DETAIL_SUCCESS',
      payload: data.data
    })
  } catch (error) {
    dispatch({
      type: 'GET_POKEMON_DETAIL_FAILURE',
      error: error,
    })
  }
}

export default actionGetPokemonDetail