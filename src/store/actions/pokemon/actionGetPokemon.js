
const actionGetPokemon = (limit) => async (dispatch) => {
  try {
    console.log(limit.limit, limit.offset);
    dispatch({
  
      type: 'GET_POKEMON_BEGIN'
    })
    const data = await fetch(`http://localhost:5005/api/v1/pokemons?limit=${limit.limit}&offset=${limit.offset}`, {
      method: 'GET', 
      headers: {
        'Content-Type': 'application/json'
      },
    });
    const commit = await data.json()

    dispatch({
      type: 'GET_POKEMON_SUCCESS',
      payload: commit.data,
      total: commit.count,
    })
  } catch (error) {
    dispatch({
      type: 'GET_POKEMON_FAILURE',
      error: error,
    })
  }
}

export default actionGetPokemon