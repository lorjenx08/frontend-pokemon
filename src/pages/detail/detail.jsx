import React from 'react'
import {connect} from 'react-redux'
import {actionGetPokemonDetail, actionSavePokemon} from '../../store/actions/pokemon'
import {useParams} from 'react-router-dom'
import {ActionDetail, InfoDetail} from '../../components/organism/detail'
import style from './detail.module.css'

const DetailPage = ({dispatchGetPokemonDetail, detail, dispatchSavePokemon, save}) => {
  const params = useParams()
  React.useEffect(() => {
    dispatchGetPokemonDetail(params.name)
  }, [dispatchGetPokemonDetail, params])
  const handleSave = React.useCallback(() =>{
    dispatchSavePokemon({ 
      name: detail?.payload?.name,
      photo_default: detail?.payload?.photo,
      photo_front: detail?.payload?.action?.front,
      photo_back: detail?.payload?.action?.back,
      abilities: JSON.stringify(detail?.payload?.abilities),
    })
    if (save.success === 'success') {
      dispatchGetPokemonDetail()
    }
  }, [dispatchSavePokemon, detail.payload, save, dispatchGetPokemonDetail])

  return (
    <div className={style.container}>
      <InfoDetail
        name={detail?.payload?.name}
        photo={detail?.payload?.photo}
        frontAction={detail?.payload?.action?.front}
        backAction={detail?.payload?.action?.back}
      />
      <ActionDetail
        name={detail?.payload?.name}
        weight={detail?.payload?.weight}
        abilities={detail?.payload?.abilities}
        handleSave={handleSave}
      />
    </div>
  )
}
const mapStateToProps = state => ({
  detail: {
    payload: state.pokemon.getDetailPoke.payload,
    loading: state.pokemon.getDetailPoke.loading,
  },
  save: {
    success: state.pokemon.saveMyPokemon.success,
    loading: state.pokemon.saveMyPokemon.loading,
  },
})

const mapDispatchToProps = dispatch => ({
  dispatchGetPokemonDetail: (name) => dispatch(actionGetPokemonDetail(name)),
  dispatchSavePokemon: (data) => dispatch(actionSavePokemon(data))
})


export default connect(mapStateToProps, mapDispatchToProps)(DetailPage)