import HomePage from "./home/home";
import DetailPage from "./detail/detail";
import MyPokemon from "./my-pokemon/my-pokemon";

export {
  HomePage,
  DetailPage,
  MyPokemon
}