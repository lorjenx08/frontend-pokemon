import React from 'react'
import {connect} from 'react-redux'
import {actionGetMyPokemon} from '../../store/actions/pokemon'
import style from './my-pokemon.module.css'

const MyPokemon = ({dispatchGetMyPokemon, mypokemon}) => {
  React.useEffect(() => {
    dispatchGetMyPokemon()
  }, [dispatchGetMyPokemon])
  return (
    <div  className={style.container}>
      <div className={style.content}>
        {mypokemon?.payload?.map(item => 
            <div key={item.id} className={style.card}>
                <div className={style.wrapperIcon}>
                  <img src={item.photo_front} alt={`icon-${item.name}`} className={style.icon}/>
                  <p className={style.textName}>{item.name}</p>
                </div>
                <div>
                </div>
                <button className={style.btn}>remove</button>
              </div>
          
          )}
      </div>
    </div>
  )
}
const mapStateToProps = state => ({
  mypokemon: {
    payload: state.pokemon.getAllMyPokemon.payload,
    loading: state.pokemon.getAllMyPokemon.loading,
  },
})

const mapDispatchToProps = dispatch => ({
  dispatchGetMyPokemon: () => dispatch(actionGetMyPokemon()),
})

export default connect(mapStateToProps, mapDispatchToProps)(MyPokemon)