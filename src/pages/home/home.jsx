import React from 'react'
import {connect} from 'react-redux'
import {actionGetPokemon} from '../../store/actions/pokemon'
import {Card} from '../../components/organism/home'
import {useNavigate} from 'react-router-dom'
import style from './home.module.css'
import fakeData from '../../module/loadingList'

const HomePage = ({dispatchGetPokemon, pokemons}) => {
  const [page, setPage] = React.useState(1)
  const navigate = useNavigate()
  const limit = 10;
  React.useEffect(() => {
    dispatchGetPokemon({
      limit,
      offset: page,
    })
  }, [page, dispatchGetPokemon,limit])
  const pokemonList = React.useMemo(() => {
    if(pokemons?.loading) {
      return fakeData
    } else {
      return pokemons?.payload
    }
  },[pokemons])
  const handleDetail = React.useCallback((name) => {
      navigate(`/detail/${name}`)
      console.log(name);
  }, [navigate])
  const max = React.useMemo(() => Math.ceil(pokemons?.total / limit), [
    pokemons.total,
    limit,
  ])
  const handleNext = React.useCallback(() => {
    setPage(page + 1)
  },[setPage, page])
  const handlePrev = React.useCallback(() => {
    setPage(page - 1)
  }, [setPage, page])
  return (
    <div className={style.container}>
      <div className={style.wrapperCard}>
      {pokemonList?.map((item, i) => 
        <Card 
          key={i} 
          name={item?.name}
          image={item?.photo}
          loading={pokemons?.loading}
          weight={item?.weight}
          handleClick={() => handleDetail(item?.name)}
        />
      )}
      </div>
      <div className={style.wrapperAction}>
        <button className={style.btnPrev}
        onClick={handlePrev}
        disabled={page === 1}
        >Prev</button>
        <div className={style.page}>
          <p>{page}...</p>
          <p>{max}</p>
        </div>
        <button className={style.btnNext} disabled={page === max} onClick={handleNext}>Next</button>
      </div>

    </div>
  )
}

const mapStateToProps = state => ({
  pokemons: {
    payload: state.pokemon.GetPokemon.payload,
    total: state.pokemon.GetPokemon.total,
    loading: state.pokemon.GetPokemon.loading,
  },
})

const mapDispatchToProps = dispatch => ({
  dispatchGetPokemon: (limit, offset) => dispatch(actionGetPokemon(limit, offset)),
})


export default connect(mapStateToProps, mapDispatchToProps)(HomePage)