import React from 'react'
import style from './info-detail.module.css'

const InfoDetail = ({ photo, name, backAction, frontAction }) => {
  const [value, setValue] = React.useState(null)
  const photoaction = React.useMemo(() => {
    if(!value) {
      return photo
    }
    return value
  }, [value, photo])
  const handleShow = React.useCallback((param) => {
    setValue(param)
  }, [setValue])
  return (
    <div className={style.content}>
      <img src={photoaction} alt={`icon-${name}`} className={style.photo} onClick={() => setValue(photo)} />
      
      <div className={style.wrapperBodyAction}>
      <div className={style.wrapActionImage} alt="" onClick={() => handleShow(backAction)}>
          <img src={backAction} alt="icon" />
      </div>
      <div className={style.wrapActionImage} alt="" onClick={() => handleShow(frontAction)}>
      <img src={frontAction} alt="icon" />
      </div>
      </div>
    </div>
  )
}

export default InfoDetail