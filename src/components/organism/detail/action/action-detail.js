import React from 'react'
import style from './action-detail.module.css'
import {connect} from 'react-redux'
import {actionGetCekPokemon} from '../../../../store/actions/pokemon'

const ActionDetail = ({name, weight, abilities, handleSave, dispatchGetPokemonCek, cek, save}) => {
  React.useEffect(() => {
    dispatchGetPokemonCek(name)
  }, [dispatchGetPokemonCek, name])
  const cekAvailable = React.useMemo(() => {
    if(cek?.payload === 1) {
      return true
    }
    return false
  }, [cek])

  return (
    <div className={style.content}>
      <p className={style.textInfo}>Name: {name}</p>
      <p className={style.textInfo}>Weight: {weight}</p>
  
        <div className={style.ability}>
        <p>Abilities: </p>
        {abilities?.map((item, i) => 
          <p className={style.textability} key={i}>{item}</p>
          )}
      </div>
      <button className={style.btnsave} disabled={!cekAvailable} onClick={handleSave}>{cekAvailable ? 'Save' : 'My Pokemon'}</button>
    </div>
  )
}
const mapStateToProps = state => ({
  cek: {
    payload: state.pokemon.cekPokemon.payload,
    loading: state.pokemon.cekPokemon.loading,
  },
  save: {
    success: state.pokemon.saveMyPokemon.success,
    loading: state.pokemon.saveMyPokemon.loading,
  },
})

const mapDispatchToProps = dispatch => ({
  dispatchGetPokemonCek: (name) => dispatch(actionGetCekPokemon(name)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ActionDetail)