import ActionDetail from "./action/action-detail";
import InfoDetail from "./info/info-detail";

export {
  ActionDetail,
  InfoDetail,
}