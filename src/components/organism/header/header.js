import React from 'react'
import style from './header.module.css'
import {Link} from 'react-router-dom'

const HeaderComponent = () => {
  return (
    <nav className={style.container}>
      <div className={style.content}>
          <div>
              <h3 className={style.title}>PokeAPI</h3>
          </div>
          <div className={style.navlink}>
            <Link to="/" className={style.textlink}>Pokemon</Link>
            <Link to="/mypokemon" className={style.textlink}>My-Pokemon</Link>
          </div>
      </div>
    </nav>
  )
}

export default HeaderComponent