import React from 'react'
import style from './layout.module.css'
import HeaderComponent from '../header/header'

const Layout = (props) => {
  const {children} = props
  return (
    <div className={style.container}>
        <HeaderComponent/>
        <div className={style.content}>
        {children}
        </div>
    </div>
  )
}

export default Layout