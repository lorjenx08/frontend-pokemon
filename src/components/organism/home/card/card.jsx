import React from 'react'
import style from './card.module.css'

const Card = ({name, image, weight, loading, handleClick}) => {
  return (
    <div className={style.card} onClick={handleClick}>
        <div className={style.wrapperBody}>
          <div>
          <p className={loading ? style.titleLoad : style.title}>{name}</p>
          <p className={loading ? style.textweightLoad : style.textweight}>weight : {weight}</p>
          </div>
          {
            loading ? 
              <div className={style.photoload} /> 
            :
            <img src={image} alt={`icon-${name}`} className={style.photo} />
          }
        </div>
    </div>
  )
}

export default Card
