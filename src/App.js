import './App.css';
import {Routes, Route} from 'react-router-dom'
import {HomePage, DetailPage, MyPokemon} from './pages'
import Layout from './components/organism/layout/layout';

function App() {
  return (
    <Layout>
      <Routes>
      <Route path='/' element={<HomePage/>} />
      <Route path='/detail/:name' element={<DetailPage/>} />
      <Route path='/mypokemon' element={<MyPokemon/>} />
      </Routes>
    </Layout>
  );
}

export default App;
